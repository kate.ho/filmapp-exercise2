package com.example.filmapp.function.ui.home.movielist;

import com.example.filmapp.model.Genre;
import com.example.filmapp.model.Movie;

import java.util.List;

public interface MovieListContract {
    interface View {
        void ShowFilmListSuccess(List<Genre> Genre, List<Movie> Movie);

        void ShowFilmListFailure(String error);
    }

    interface Presenter {
        void LoadMovie();
    }
}
