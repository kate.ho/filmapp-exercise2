package com.example.filmapp.data.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.filmapp.model.Genre;
import com.example.filmapp.model.Movie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DBManager extends SQLiteOpenHelper {
    private final String TAG = "DBManager";
    private static final String DATABASE_NAME = "movies_manager";
    private static final String ID = "id";

    // Movie table
    private static final String TABLE_MOVIE = "movie";
    private static final String TITLE = "title";
    private static final String POSTER_PATH = "posterPath";
    private static final String RELEASE_DATE = "releaseDate";
    private static final String RATTING = "ratting";
    private static final String GENDERS_ID = "genreIds";
    private static final String BACKDROP_PATH = "backdrop_path";
    private static final String OVERVIEW = "overview";
    private String SQLQueryCreateMovie = "CREATE TABLE " + TABLE_MOVIE + " (" +
            ID + " integer primary key, " +
            TITLE + " TEXT, " +
            POSTER_PATH + " TEXT, " +
            RELEASE_DATE + " TEXT, " +
            RATTING + " float, " +
            GENDERS_ID + " TEXT)";

    // Genre table
    private static final String TABLE_GENRE = "genre";
    private static final String NAME = "name";
    private static int VERSION = 1;
    private Context context;
    private  String SLQueryCreateGenre = "CREATE TABLE " + TABLE_GENRE + " (" +
            ID + " integer primary key, " +
            NAME + " TEXT)";

    public DBManager(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQLQueryCreateMovie);
        db.execSQL(SLQueryCreateGenre);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //Add data of movie into movie table on sqlite
    public void addMovie(Movie movie) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ID, movie.getId());
        values.put(TITLE, movie.getTitle());
        values.put(POSTER_PATH, movie.getPosterPath());
        values.put(RELEASE_DATE, movie.getReleaseDate());
        values.put(GENDERS_ID, convertArrayToStringMethod(movie.getGenreIds()));
        values.put(RATTING, movie.getRating());
        db.insert(TABLE_MOVIE, null, values);
        db.close();
        Log.d(TAG, "add movie Successfuly");
    }

    //Add data of genre into genre table on sqlite
    public void addGenre(Genre genre){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ID, genre.getId());
        values.put(NAME, genre.getName());
        db.insert(TABLE_GENRE, null, values);
        db.close();
        Log.d(TAG, "add genre Successfuly");
    }

    //Get data on movies table from sqlite
    public List<Movie> getAllMovie() {
        List<Movie> listMovie = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_MOVIE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        if (cursor.moveToFirst()) {
            do {
                Movie movie = new Movie();
                movie.setId(cursor.getInt(0));
                movie.setTitle(cursor.getString(1)+"");
                movie.setPosterPath(cursor.getString(2));
                movie.setReleaseDate(cursor.getString(3));
                movie.setGenreIds(convertStringToArrayMethod(cursor.getString(5)));
                movie.setRating(cursor.getFloat(4));
                listMovie.add(movie);

            } while (cursor.moveToNext());
        }
        db.close();
        return listMovie;
    }

    public List<Genre> getALlGenre() {
        List<Genre> listGenre = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_GENRE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        if (cursor.moveToFirst()) {
            do {
                Genre genre = new Genre();
                genre.setId(cursor.getInt(0));
                genre.setName(cursor.getString(1)+"");
                listGenre.add(genre);

            } while (cursor.moveToNext());
        }
        db.close();
        return listGenre;
    }

    //Convert List<Integer> to String
    public static String convertArrayToStringMethod(List<Integer> strArray) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < strArray.size(); i++) {
            if (i == strArray.size()-1){
                stringBuilder.append(strArray.get(i));
            }else {
                stringBuilder.append(strArray.get(i) + ",");
            }
        }
        return stringBuilder.toString();
    }

    // covert String to List<Interger>
    public static List<Integer> convertStringToArrayMethod(String mtext){
        List<Integer> mIntList = new ArrayList<>();
        List<String> myList = new ArrayList<String>(Arrays.asList(mtext.split(",")));
        for(String text : myList) mIntList.add(Integer.valueOf(text));
        return mIntList;
    }
}
