package com.example.filmapp.function.ui.home.movielist;

import com.example.filmapp.data.api.TMDbApi;
import com.example.filmapp.model.Genre;
import com.example.filmapp.model.GenresResponse;
import com.example.filmapp.model.Movie;
import com.example.filmapp.model.MoviesResponse;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieListPresenter implements MovieListContract.Presenter {

    private MovieListContract.View mView;
    private static final String URL = "https://api.themoviedb.org/";
    private static final String API_KEY = "2b77ea2ab91650a8b5e07663cd6546cd";
    private Retrofit mRetrofits;
    private static final String LANGUAGE = "en-US";

    public MovieListPresenter(MovieListContract.View view) {
        mView = view;
    }

    @Override
    public void LoadMovie() {
        mRetrofits = new Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
        getGenres();
    }

    //get genre data from api
    public void getGenres() {
        TMDbApi tmDbApi = mRetrofits.create(TMDbApi.class);
        Call<GenresResponse> call = tmDbApi.getGenres(API_KEY,LANGUAGE);
        call.enqueue(new Callback<GenresResponse>() {
            @Override
            public void onResponse(Call<GenresResponse> call, Response<GenresResponse> response) {
                List<Genre> genreList = response.body().getGenres();
                getMovies(genreList);
            }

            @Override
            public void onFailure(Call<GenresResponse> call, Throwable t) {
                mView.ShowFilmListFailure(t.getMessage());
            }
        });
    }

    // get movie data from api
    public void getMovies( List<Genre> genreList) {
       TMDbApi tmDbApi = mRetrofits.create(TMDbApi.class);
       Call<MoviesResponse> call = tmDbApi.getPopularMovies(API_KEY,LANGUAGE,1);
       call.enqueue(new Callback<MoviesResponse>() {
           @Override
           public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
               List<Movie> movieList = response.body().getMovies();
               mView.ShowFilmListSuccess(genreList, movieList);
           }

           @Override
           public void onFailure(Call<MoviesResponse> call, Throwable t) {
               mView.ShowFilmListFailure(t.getMessage());
           }
       });
    }
}
