package com.example.filmapp.function.ui.home.movielist.moviedetail;

import com.example.filmapp.model.Movie;

public interface MovieDetailContract {
    interface View {
        void ShowMovieDetailSuccess(Movie movie);

        void ShowMovieDetailFailure(String error);
    }

    interface Presenter {
        void LoadMovieDetail(int mMovieID);
    }
}
