package com.example.filmapp.function.ui.home.movielist.moviedetail;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.filmapp.R;
import com.example.filmapp.common.Define;
import com.example.filmapp.model.Genre;
import com.example.filmapp.model.Movie;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieDetailActivity extends AppCompatActivity implements MovieDetailContract.View{

    private static String IMAGE_BASE_URL = "http://image.tmdb.org/t/p/w780";
    private int movieId;
    private MovieDetailPresenter mMovieDetailPresenters;

    @BindView(R.id.img_movie_backgroud)
    ImageView movieBackdrop;
    @BindView(R.id.tv_movieDetails_Title)
    TextView movieTitle;
    @BindView(R.id.tv_movieDetails_Genres)
     TextView movieGenres;
    @BindView(R.id.tv_movieDetails_Overview)
    TextView movieOverview;
    @BindView(R.id.tv_movieDetails_ReleaseDate)
    TextView movieReleaseDate;
    @BindView(R.id.tv_movieDetails_Rating)
    RatingBar movieRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        ButterKnife.bind(this);

        movieId = getIntent().getIntExtra(Define.MOVIE_ID, movieId);
        initPresenter();
    }

    private void initPresenter() {
        mMovieDetailPresenters = new MovieDetailPresenter(this);
        mMovieDetailPresenters.LoadMovieDetail(movieId);
    }

    @Override
    public void ShowMovieDetailSuccess(Movie movie) {
        movieTitle.setText(movie.getTitle().toString());
        movieOverview.setText(movie.getOverview().toString());
        movieRating.setRating(movie.getRating() / 2);
        movieReleaseDate.setText(movie.getReleaseDate());
        if (!isFinishing()) {
            Glide.with(MovieDetailActivity.this)
                    .load(IMAGE_BASE_URL + movie.getBackdrop())
                    .apply(RequestOptions.placeholderOf(R.color.colorPrimary))
                    .into(movieBackdrop);
        }
        movieGenres.setText(getGenresText(movie.getGenres()));
    }

    @Override
    public void ShowMovieDetailFailure(String error) {

    }

    //Convert genre name to string
    private String getGenresText(List<Genre> genreIds) {
        List<String> movieGenres = new ArrayList<>();
        for (Genre genre : genreIds) {
            movieGenres.add(genre.getName());
        }
        return TextUtils.join(", ", movieGenres);
    }
}
