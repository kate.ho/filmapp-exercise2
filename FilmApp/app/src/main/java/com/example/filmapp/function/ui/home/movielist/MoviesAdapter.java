package com.example.filmapp.function.ui.home.movielist;

import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.filmapp.R;
import com.example.filmapp.model.Genre;
import com.example.filmapp.model.Movie;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieViewHolder> {
    private List<Movie> movies;
    private List<Genre> allGenres;
    private static OnItemClickListener listener;
    private String IMAGE_BASE_URL = "http://image.tmdb.org/t/p/w500";

    public MoviesAdapter(List<Movie> movies, List<Genre> allGenres, OnItemClickListener listener) {
        this.movies = movies;
        this.allGenres = allGenres;
        this.listener = listener;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie_list, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        holder.bind(movies.get(position));
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    class MovieViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_movie_poster)
        ImageView imgPoster;
        @BindView(R.id.tv_movie_release_date)
        TextView tvReleaseDate;
        @BindView(R.id.tv_movie_title)
        TextView tvTitle;
        @BindView(R.id.tv_movie_rating)
        TextView tvRating;
        @BindView(R.id.tv_movie_genre)
        TextView tvGenres;

        public MovieViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onItemClick(itemView, getLayoutPosition(), movies);
                }
            });
        }

        public void bind(Movie movie) {
            tvReleaseDate.setText(movie.getReleaseDate().split("-")[0]);
            tvTitle.setText(movie.getTitle());
            tvRating.setText(String.valueOf(movie.getRating()));
            tvGenres.setText(getGenres(movie.getGenreIds()));

            Uri uri = Uri.parse(IMAGE_BASE_URL + movie.getPosterPath());
            Glide.with(itemView.getContext())
                    .load(uri)
                    .into(imgPoster);
        }

        private String getGenres(List<Integer> genreIds) {
            List<String> movieGenres = new ArrayList<>();
            for (Integer genreId : genreIds) {
                for (Genre genre : allGenres) {
                    if (genre.getId() == genreId) {
                        movieGenres.add(genre.getName());
                        break;
                    }
                }
            }
            return TextUtils.join(", ", movieGenres);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position, List<Movie> movies);
    }
}
