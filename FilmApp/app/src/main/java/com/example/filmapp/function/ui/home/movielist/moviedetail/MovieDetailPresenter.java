package com.example.filmapp.function.ui.home.movielist.moviedetail;

import com.example.filmapp.data.api.TMDbApi;
import com.example.filmapp.model.Movie;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieDetailPresenter implements MovieDetailContract.Presenter {

    private MovieDetailContract.View mView;
    private static final String URL = "https://api.themoviedb.org/";
    private static final String API_KEY = "2b77ea2ab91650a8b5e07663cd6546cd";
    private Retrofit mRetrofits;
    private static final String LANGUAGE = "en-US";

    public MovieDetailPresenter(MovieDetailContract.View view) {
        mView = view;
    }

    @Override
    public void LoadMovieDetail(int mMovieID) {
        mRetrofits = new Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/3/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        getMovieDetail(mMovieID);
    }

    public void getMovieDetail(int mMovieID) {
        TMDbApi tmDbApi = mRetrofits.create(TMDbApi.class);
        Call<Movie> call = tmDbApi.getMovie(mMovieID,API_KEY,LANGUAGE);
        call.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                Movie movie = response.body();
                mView.ShowMovieDetailSuccess(movie);
            }
            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                mView.ShowMovieDetailFailure(t.getMessage());
            }
        });
    }
}
