package com.example.filmapp.function.ui.home.movielist;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.filmapp.R;
import com.example.filmapp.common.Checkconnection;
import com.example.filmapp.common.Define;
import com.example.filmapp.common.NetworkChangeReceiver;
import com.example.filmapp.data.sqlite.DBManager;
import com.example.filmapp.function.ui.home.movielist.moviedetail.MovieDetailActivity;
import com.example.filmapp.model.Genre;
import com.example.filmapp.model.Movie;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieActivity extends AppCompatActivity implements MovieListContract.View, MoviesAdapter.OnItemClickListener {

    @BindView(R.id.rcv_movies_list)
    RecyclerView rcvFilmList;

    private DBManager mDBManagers;
    private MovieListPresenter mFilmListPresenters;
    private MoviesAdapter mMoviesAdapters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        ButterKnife.bind(this);
        mDBManagers = new DBManager(this);
        if (Checkconnection.haveNetworkConnection(getApplicationContext())){
            initPresenter();
        }else {
            mMoviesAdapters = new MoviesAdapter(mDBManagers.getAllMovie(),mDBManagers.getALlGenre(),this);
            rcvFilmList.setAdapter(mMoviesAdapters);
        }
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rcvFilmList.setLayoutManager(mLayoutManager);
    }

    private void initPresenter() {
        mFilmListPresenters = new MovieListPresenter(this);
        mFilmListPresenters.LoadMovie();
    }

    @Override
    public void ShowFilmListSuccess(List<Genre> genreList, List<Movie> movieList) {
        addDataMovie(movieList);
        addDataGenre(genreList);
        mMoviesAdapters = new MoviesAdapter(mDBManagers.getAllMovie(),mDBManagers.getALlGenre(),this);
        rcvFilmList.setAdapter(mMoviesAdapters);
    }

    @Override
    public void ShowFilmListFailure(String error) {
        Log.d("erro", error);
    }

    //Click item on list of movie (recycleview)
    @Override
    public void onItemClick(View itemView, int position, List<Movie> movies) {
        if (Checkconnection.haveNetworkConnection(getApplicationContext())){
            Movie movie = movies.get(position);
            Intent intent = new Intent(MovieActivity.this, MovieDetailActivity.class);
            intent.putExtra(Define.MOVIE_ID, movie.getId());
            startActivity(intent);
        }else {
            Checkconnection.ShowToast_Short(getApplicationContext(),"Please check your internet");
        }

    }

    //add genre data from api to movie table on sqlite
    public void addDataMovie(List<Movie> movieList){
        for (int i = 0; i < movieList.size(); i++){
            Movie movie = movieList.get(i);
            mDBManagers.addMovie(movie);
        }
    }

    //add genre data from api to movie table on sqlite
    public void addDataGenre(List<Genre> genreList){
        for (int i = 0; i < genreList.size(); i++){
            Genre genre = genreList.get(i);
            mDBManagers.addGenre(genre);
        }
    }
}
