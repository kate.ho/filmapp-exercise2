package com.example.filmapp.function.ui.home;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.filmapp.R;
import com.example.filmapp.common.NetworkChangeReceiver;
import com.example.filmapp.function.ui.home.movielist.MovieActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    private NetworkChangeReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initBroadcastReceiver();
    }

    @OnClick(R.id.btn_filmlist)
    public void onFilmListClick(View view){
        Intent intent = new Intent(this, MovieActivity.class);
        startActivity(intent);
    }

    private void initBroadcastReceiver(){
        receiver = new NetworkChangeReceiver();
        final IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(receiver, filter);
    }
}
