package com.example.filmapp.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class NetworkChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Checkconnection.haveNetworkConnection(context)){
            Toast.makeText(context, "Back online", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context, "No internet", Toast.LENGTH_SHORT).show();
        }
    }
}
